﻿using DevTrends.MvcDonutCaching;
using System.Web.Mvc;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace UmbracoDemo.Controllers
{
    public class DefaultController : RenderMvcController
    {
        [DonutOutputCache(CacheProfile = "OneDay")]
        public override ActionResult Index(RenderModel model)
        {
            return CurrentTemplate(model);
        }
    }
}