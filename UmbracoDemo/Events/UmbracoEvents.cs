﻿using DevTrends.MvcDonutCaching;
using System;
using Umbraco.Core;
using Umbraco.Core.Logging;
using Umbraco.Core.Services;
using Umbraco.Web.Mvc;
using UmbracoDemo.Controllers;

namespace UmbracoDemo.Events
{
    public class UmbracoEvents : ApplicationEventHandler
    {
        protected override void ApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            ContentService.Published += ContentService_Published;

            DefaultRenderMvcControllerResolver.Current.SetDefaultControllerType(typeof(DefaultController));
        }

        private void ContentService_Published(Umbraco.Core.Publishing.IPublishingStrategy sender, Umbraco.Core.Events.PublishEventArgs<Umbraco.Core.Models.IContent> e)
        {
            ClearCache();
        }

        private void ClearCache()
        {
            try
            {
                var cacheManager = new OutputCacheManager();
                cacheManager.RemoveItems();
            }
            catch (Exception ex)
            {
                LogHelper.Error<UmbracoEvents>(string.Format("Exception: {0} - StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
        }
    }
}